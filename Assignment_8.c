#include <stdio.h>
struct student {
    char fName[50];
    char subj[50];
    int indexno;
    float marks;
} 
s[10];

int main() {
    int i,stucount;
    printf("...Student Information System...\n\n");
    
	printf("Enter the number of students : ");
	scanf("%d",&stucount);

    
    printf("\n..Enter Student Details...\n");
    for (i = 0; i < stucount; ++i) {
        s[i].indexno = i + 1;
        printf("\nDetails Of Student number %d,\n", s[i].indexno);
        printf("\tEnter First Name : ");
        scanf("%s", s[i].fName);
        printf("\tEnter Subject : ");
        scanf("%s", s[i].subj);
        printf("\tEnter marks: ");
        scanf("%f", &s[i].marks);
    }
    printf("\nDisplaying Information of Students\n\n");

    
    for (i = 0; i < stucount; ++i) {
        printf("\nStudent Number: %d\n", i + 1);
        printf("\tStudent First name: ");
        puts(s[i].fName);
        printf("\tSubject: ");
        puts(s[i].subj);
        printf("\tMarks: %.1f", s[i].marks);
        printf("\n");
    }
    return 0;
}
